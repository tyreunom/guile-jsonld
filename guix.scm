;;;; Copyright (C) 2019 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(use-modules
  ((guix licenses) #:prefix license:)
  (guix build-system gnu)
  (guix download)
  (guix git-download)
  (guix packages)
  (guix utils)
  (gnu packages autotools)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages pkg-config)
  (gnu packages texinfo)
  (gnu packages tls))

(package
  (name "guile-jsonld")
  (version "1.0.1")
  (source
    (origin
      (method git-fetch)
      (uri (git-reference
             (url "https://framagit.org/tyreunom/guile-jsonld")
             (commit version)))
      (file-name (git-file-name name version))
      (sha256
       (base32
        "0zfn3nwlz6xzip1j8xbj768dc299r037cfc81bk6kwl9xhzkjbrg"))))
  (build-system gnu-build-system)
  (arguments
   `(#:tests? #f)); require network
  (inputs
   `(("guile" ,guile-3.0)
     ("guile-gnutls" ,gnutls)
     ("guile-json" ,guile-json-4)
     ("guile-rdf" ,guile-rdf)))
  (native-inputs
   `(("automake" ,automake)
     ("autoconf" ,autoconf)
     ("pkg-config" ,pkg-config)
     ("texinfo" ,texinfo)))
  (home-page "https://framagit.org/tyreunom/guile-jsonld")
  (synopsis "Guile implementation of the JsonLD API specification")
  (description "Guile JsonLD is an implementation of the JsonLD API defined
by the W3C for GNU Guile.  JsonLD stands for Json for Linked Data.  Linked Data
is a representation for the semantic web.  It allows you to express links
between data, in a way that is very similar to WikiData for instance.  An object
can have relations (in the form of an IRI) that relates it to one or more objects
or strings, represented by a Json object or an IRI.")
  (license license:gpl3+))
