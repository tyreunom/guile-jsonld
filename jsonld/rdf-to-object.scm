;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (jsonld rdf-to-object)
  #:use-module (iri iri)
  #:use-module (jsonld deserialize-jsonld)
  #:use-module (jsonld json)
  #:use-module (json)
  #:use-module ((rdf rdf) #:hide (blank-node?))
  #:use-module ((rdf xsd) #:prefix xsd:)
  #:use-module (srfi srfi-1)
  #:export (rdf-to-object))

(define* (rdf-to-object value rdf-direction use-native-types?
                        #:key processing-mode)
  (if (or (blank-node? value) (absolute-iri? value))
      `(("@id" . ,value))
      ;; 2
      (let ((result '())
            (converted-value (rdf-literal-lexical-form value))
            (datatype (rdf-literal-type value))
            (type json-null))
        (cond
          ;; 2.4
          (use-native-types?
            (cond
              ;; 2.4.1
              ((equal? datatype (xsd-iri "string"))
               (set! converted-value (rdf-literal-lexical-form value)))
              ;; 2.4.2
              ((equal? datatype (xsd-iri "boolean"))
               (cond
                 ((equal? (rdf-literal-lexical-form value) "true")
                  (set! converted-value #t))
                 ((equal? (rdf-literal-lexical-form value) "false")
                  (set! converted-value #f))
                 (else
                   (set! converted-value (rdf-literal-lexical-form value))
                   (set! type (xsd-iri "boolean")))))
              ;; 2.4.3
              ((and (equal? datatype (xsd-iri "integer"))
                    ((rdf-datatype-lexical? xsd:integer) converted-value))
               (set! converted-value ((rdf-datatype-lexical->value xsd:integer)
                                      converted-value)))
              ((and (equal? datatype (xsd-iri "double"))
                    ((rdf-datatype-lexical? xsd:double) converted-value))
               (set! converted-value ((rdf-datatype-lexical->value xsd:double)
                                      converted-value)))
              (else
                (set! type datatype))))
          ;; 2.5
          ((and (not (processing-mode-1.0? processing-mode))
                (equal? datatype (rdf-iri "JSON")))
           (set! type "@json")
           (catch #t
             (lambda _
               (set! converted-value (json-string->scm (rdf-literal-lexical-form value))))
             (lambda _
               (throw 'invalid-json-literal))))
          ;; 2.6
          ((and (> (string-length datatype) (string-length "https://www.w3.org/ns/i18n#"))
                (equal? (substring datatype 0 (string-length "https://www.w3.org/ns/i18n#"))
                        "https://www.w3.org/ns/i18n#")
                (equal? rdf-direction "i18n-datatype"))
           ;; 2.6.1
           (set! converted-value (rdf-literal-lexical-form value))
           (let* ((fragment (substring datatype (string-length "https://www.w3.org/ns/i18n#")))
                  (fragment (string-split fragment #\_))
                  (language (car fragment))
                  (direction (cadr fragment)))
             ;; 2.6.2
             (unless (equal? language "")
               (set! result (alist-set result "@language" language)))
             ;; 2.6.3
             (unless (equal? direction "")
               (set! result (alist-set result "@direction" direction)))))
          ;; 2.7
          ((rdf-literal-langtag value)
           (set! result (alist-set result "@language" (rdf-literal-langtag value))))
          ;; 2.8
          (else
            (unless (equal? datatype (xsd-iri "string"))
              (set! type datatype))))
        ;; 2.9
        (set! result (alist-set result "@value" converted-value))
        ;; 2.10
        (when (not-null-or-false type)
          (set! result (alist-set result "@type" type)))
        ;; 2.11
        result)))
