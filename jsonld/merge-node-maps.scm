;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (jsonld merge-node-maps)
  #:use-module (jsonld json)
  #:export (merge-node-maps))

(define* (merge-node-maps graph-map)
  ;; 1
  (let ((result '()))
    (for-each-pair
      (lambda (graph-name node-map)
        (for-each-pair
          (lambda (id node)
            (let ((merged-node (or (assoc-ref result id) `(("@id" . ,id)))))
              (for-each-pair
                (lambda (property values)
                  (if (json-keyword? property)
                      (set! merged-node
                        (alist-set merged-node property values))
                      (set! merged-node
                        (alist-set
                          merged-node property
                          (list->array 1
                            (append
                              (if (assoc-ref merged-node property)
                                  (array->list (assoc-ref merged-node property))
                                  '())
                              (if (json-array? values)
                                (array->list values)
                                (list values))))))))
                node)
              (set! result (alist-set result id merged-node))))
          node-map))
      graph-map)
    result))
