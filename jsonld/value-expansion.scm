;;;; Copyright (C) 2019, 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (jsonld value-expansion)
  #:use-module (jsonld context)
  #:use-module (jsonld iri-expansion)
  #:use-module (jsonld json)
  #:use-module (jsonld options)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (web uri)
  #:export (value-expansion))

(define* (value-expansion active-context active-property value
                          #:key (options (new-jsonld-options)))
  "Expand a value.  This is an implementation of the value expansion algorithm
defined in the JsonLD API specification.

See @url{https://www.w3.org/TR/json-ld-api/#value-expansion}."
  (let* ((definition (term-definition-ref active-context active-property))
         (type-mapping
           (if (term-definition? definition)
               (term-definition-type definition)
               #f))
         (language-mapping
           (if (term-definition? definition)
               (term-definition-language definition)
               #f))
         (direction-mapping
           (if (term-definition? definition)
               (term-definition-direction definition)
               #f)))
    (cond
      ;; 1
      ((and (string? value)
            (equal? type-mapping "@id"))
        (let ((result (iri-expansion active-context value
                                     #:document-relative? #t
                                     #:options options)))
          `(("@id" . ,(assoc-ref result "iri")))))
      ;; 2
      ((and (string? value)
            (equal? type-mapping "@vocab"))
        (let ((result (iri-expansion active-context value
                                     #:document-relative? #t
                                     #:vocab? #t
                                     #:options options)))
          `(("@id" . ,(assoc-ref result "iri")))))
      (else
        ;; 3
        (let ((result `(("@value" . ,value))))
          ;; 4
          (when (and type-mapping (not (member type-mapping '("@id" "@vocab" "@none"))))
            (set! result (alist-set result "@type" type-mapping)))
          ;; 5
          (when (string? value)
            (let* ((language (or (not-null-or-false language-mapping)
                                 (active-context-language active-context)))
                   (direction (or (not-null-or-false direction-mapping)
                                  (active-context-direction active-context))))
              (when (json-null? language-mapping)
                (set! language json-null))
              (when (json-null? direction-mapping)
                (set! direction json-null))
              (when (not-null-or-false language)
                (set! result (alist-set result "@language" language)))
              (when (not-null-or-false direction)
                (set! result (alist-set result "@direction" direction)))))
          ;; 6
          result)))))
