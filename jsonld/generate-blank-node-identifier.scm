;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (jsonld generate-blank-node-identifier)
  #:use-module (jsonld json)
  #:export (get-generate-blank-node-identifier))

(define (get-generate-blank-node-identifier)
  (define identifier-map '())
  (define counter 0)

  (define* (generate-blank-node-identifier identifier)
      ;; 1
      (if (and (not (json-null? identifier))
               (json-has-key? identifier-map identifier))
       (assoc-ref identifier-map identifier)
       (let ((blank-node-identifier
               (string-append "_:b" (number->string counter))))
         (set! counter (+ counter 1))
         (unless (json-null? identifier)
           (set! identifier-map
             (alist-set identifier-map identifier blank-node-identifier)))
         blank-node-identifier)))

  generate-blank-node-identifier)
