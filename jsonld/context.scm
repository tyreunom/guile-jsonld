;;;; Copyright (C) 2019, 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (jsonld context)
  #:use-module (ice-9 match)
  #:use-module (jsonld json)
  #:use-module (srfi srfi-9)
  #:export (make-active-context
            active-context?
            active-context-definitions
            active-context-base
            active-context-original-base
            active-context-inverse-context
            active-context-vocab
            active-context-language
            active-context-direction
            active-context-previous
            new-active-context
            update-active-context

            make-term-definition
            term-definition?
            term-definition-iri
            term-definition-base-url
            term-definition-reverse?
            term-definition-type
            term-definition-language
            term-definition-direction
            term-definition-context
            term-definition-nest
            term-definition-prefix?
            term-definition-index
            term-definition-protected?
            term-definition-container
            term-definition-equal?
            new-term-definition
            update-term-definition
            term-definition-ref))

;;; Section 4.1: definitions of an Active Context and a Term Definition

(define-record-type active-context
  (make-active-context definitions base original-base inverse-context vocab
                       language direction previous)
  active-context?
  (definitions     active-context-definitions)
  (base            active-context-base)
  (original-base   active-context-original-base)
  (inverse-context active-context-inverse-context)
  (vocab           active-context-vocab)
  (language        active-context-language)
  (direction       active-context-direction)
  (previous        active-context-previous))

(define* (update-active-context
           active-context
           #:key (definitions     (active-context-definitions     active-context))
                 (base            (active-context-base            active-context))
                 (original-base   (active-context-original-base   active-context))
                 (inverse-context (active-context-inverse-context active-context))
                 (vocab           (active-context-vocab           active-context))
                 (language        (active-context-language        active-context))
                 (direction       (active-context-direction       active-context))
                 (previous        (active-context-previous        active-context)))
  (make-active-context definitions base original-base inverse-context vocab
                       language direction previous))

(define* (new-active-context
           #:key (definitions     '())
                 (base            #f)
                 (original-base   #f)
                 (inverse-context json-null)
                 (vocab           #f)
                 (language        #f)
                 (direction       #f)
                 (previous        json-null))
  (make-active-context definitions base original-base inverse-context vocab
                       language direction previous))

(define-record-type term-definition
  (make-term-definition iri prefix? protected? reverse? base-url context container
                        direction index language nest type)
  term-definition?
  (iri        term-definition-iri)
  (prefix?    term-definition-prefix?)
  (protected? term-definition-protected?)
  (reverse?   term-definition-reverse?)
  (base-url   term-definition-base-url)
  (context    term-definition-context)
  (container  term-definition-container)
  (direction  term-definition-direction)
  (index      term-definition-index)
  (language   term-definition-language)
  (nest       term-definition-nest)
  (type       term-definition-type))

(define* (update-term-definition
           term-definition
           #:key (iri        (term-definition-iri        term-definition))
                 (prefix?    (term-definition-prefix?    term-definition))
                 (protected? (term-definition-protected? term-definition))
                 (reverse?   (term-definition-reverse?   term-definition))
                 (base-url   (term-definition-base-url   term-definition))
                 (context    (term-definition-context    term-definition))
                 (container  (term-definition-container  term-definition))
                 (direction  (term-definition-direction  term-definition))
                 (index      (term-definition-index      term-definition))
                 (language   (term-definition-language   term-definition))
                 (nest       (term-definition-nest       term-definition))
                 (type       (term-definition-type       term-definition)))
  (make-term-definition iri prefix? protected? reverse? base-url context container
                        direction index language nest type))

(define* (new-term-definition
           #:key (iri        #f)
                 (prefix?    #f)
                 (protected? #f)
                 (reverse?   #f)
                 (base-url   #f)
                 (context    #f)
                 (container  #f)
                 (direction  #f)
                 (index      #f)
                 (language   #f)
                 (nest       #f)
                 (type       #f))
  (make-term-definition iri prefix? protected? reverse? base-url context container
                        direction index language nest type))

(define (term-definition-equal? a b)
  (match a
    (($ term-definition iri prefix? protected? reverse? base-url context
        container direction index language nest type)
     (and (term-definition? b)
          (equal? iri        (term-definition-iri        b))
          (equal? prefix?    (term-definition-prefix?    b))
          (equal? protected? (term-definition-protected? b))
          (equal? reverse?   (term-definition-reverse?   b))
          (equal? base-url   (term-definition-base-url   b))
          (equal? context    (term-definition-context    b))
          (equal? container  (term-definition-container  b))
          (equal? direction  (term-definition-direction  b))
          (equal? index      (term-definition-index      b))
          (equal? language   (term-definition-language   b))
          (equal? nest       (term-definition-nest       b))
          (equal? type       (term-definition-type       b))))
    (_ #f)))

(define (term-definition-ref context term)
  (let ((definitions (active-context-definitions context)))
    (assoc-ref definitions term)))
