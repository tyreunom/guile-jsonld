;;;; Copyright (C) 2019, 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (jsonld options)
  #:use-module (iri iri)
  #:use-module (json)
  #:use-module (jsonld download)
  #:use-module (jsonld memoization)
  #:use-module (web client)
  #:use-module (web response)
  #:use-module (web uri)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:export (jsonld-options?
            jsonld-options-base
            jsonld-options-compact-arrays?
            jsonld-options-compact-to-relative?
            jsonld-options-document-loader
            jsonld-options-expand-context
            jsonld-options-extract-all-scripts?
            jsonld-options-frame-expansion?
            jsonld-options-ordered?
            jsonld-options-processing-mode
            jsonld-options-produce-generalized-rdf?
            jsonld-options-rdf-direction
            jsonld-options-use-native-types?
            jsonld-options-use-rdf-type?
            new-jsonld-options
            update-jsonld-options))

(define-record-type jsonld-options
  (make-jsonld-options
    base compact-arrays? compact-to-relative? document-loader expand-context
    extract-all-scripts? frame-expansion? ordered? processing-mode
    produce-generalized-rdf? rdf-direction use-native-types? use-rdf-type?)
  jsonld-options?
  (base                     jsonld-options-base)
  (compact-arrays?          jsonld-options-compact-arrays?)
  (compact-to-relative?     jsonld-options-compact-to-relative?)
  (document-loader          jsonld-options-document-loader)
  (expand-context           jsonld-options-expand-context)
  (extract-all-scripts?     jsonld-options-extract-all-scripts?)
  (frame-expansion?         jsonld-options-frame-expansion?)
  (ordered?                 jsonld-options-ordered?)
  (processing-mode          jsonld-options-processing-mode)
  (produce-generalized-rdf? jsonld-options-produce-generalized-rdf?)
  (rdf-direction            jsonld-options-rdf-direction)
  (use-native-types?        jsonld-options-use-native-types?)
  (use-rdf-type?            jsonld-options-use-rdf-type?))

(define* (new-jsonld-options #:key
                             (base #f)
                             (compact-arrays? #t)
                             (compact-to-relative? #t)
                             (document-loader (memoize download-json))
                             (expand-context #f)
                             (extract-all-scripts? #f)
                             (frame-expansion? #f)
                             (ordered? #f)
                             (processing-mode "json-ld-1.1")
                             (produce-generalized-rdf? #t)
                             (rdf-direction #f)
                             (use-native-types? #f)
                             (use-rdf-type? #f))
  (make-jsonld-options
    base compact-arrays? compact-to-relative? document-loader expand-context
    extract-all-scripts? frame-expansion? ordered? processing-mode
    produce-generalized-rdf? rdf-direction use-native-types? use-rdf-type?))

(define* (update-jsonld-options options #:key
           (base                     (jsonld-options-base options))
           (compact-arrays?          (jsonld-options-compact-arrays? options))
           (compact-to-relative?     (jsonld-options-compact-to-relative? options))
           (document-loader          (jsonld-options-document-loader options))
           (expand-context           (jsonld-options-expand-context options))
           (extract-all-scripts?     (jsonld-options-extract-all-scripts? options))
           (frame-expansion?         (jsonld-options-frame-expansion? options))
           (ordered?                 (jsonld-options-ordered? options))
           (processing-mode          (jsonld-options-processing-mode options))
           (produce-generalized-rdf? (jsonld-options-produce-generalized-rdf? options))
           (rdf-direction            (jsonld-options-rdf-direction options))
           (use-native-types?        (jsonld-options-use-native-types? options))
           (use-rdf-type?            (jsonld-options-use-rdf-type? options)))
  (make-jsonld-options
    base compact-arrays? compact-to-relative? document-loader expand-context
    extract-all-scripts? frame-expansion? ordered? processing-mode
    produce-generalized-rdf? rdf-direction use-native-types? use-rdf-type?))
