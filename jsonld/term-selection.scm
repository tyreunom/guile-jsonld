;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (jsonld term-selection)
  #:use-module (jsonld context)
  #:use-module (jsonld inverse-context-creation)
  #:use-module (jsonld json)
  #:export (term-selection))

(define* (term-selection active-context var containers type/language
                         preferred-values)
  ;; 1
  (when (json-null? (active-context-inverse-context active-context))
    (set! active-context
      (update-active-context active-context
                             #:inverse-context
                             (inverse-context-creation active-context))))
  (let ((container-map (assoc-ref (active-context-inverse-context active-context) var))
        (result json-null))
    ;; 4
    (for-each
      (lambda (container)
        ;; 4.1
        (when (json-has-key? container-map container)
          ;; 4.2
          (let* ((type/language-map (assoc-ref container-map container))
                 ;; 4.3
                 (value-map (assoc-ref type/language-map type/language)))
            (for-each
              (lambda (item)
                (when (json-has-key? value-map item)
                  (unless (not-null-or-false result)
                    (set! result (assoc-ref value-map item)))))
              preferred-values))))
      containers)
    ;; 5
    result))
