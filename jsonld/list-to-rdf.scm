;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (jsonld list-to-rdf)
  #:use-module (iri iri)
  #:use-module (jsonld deserialize-jsonld)
  #:use-module (jsonld json)
  #:use-module (jsonld object-to-rdf)
  #:use-module (rdf rdf)
  #:export (list-to-rdf))

(define* (list-to-rdf generate-blank-node rdf-direction lst list-triples)
  (let ((result json-null))
    (if (null? lst)
        ;; 1
        (set! result (rdf-iri "nil"))
        ;; 2
        (let ((bnodes (map
                        (lambda _
                          (blank-node->rdf-blank-node (generate-blank-node json-null)))
                        lst)))
          ;; 3
          (let loop ((bnodes bnodes) (lst lst))
            (unless (null? bnodes)
              ;; 3.2
              (let* ((subject (car bnodes))
                     (item (car lst))
                     (rest (cdr bnodes))
                     (rest (if (null? rest) (rdf-iri "nil") (car rest)))
                     (res (object-to-rdf generate-blank-node rdf-direction
                                         item '()))
                     (object (assoc-ref res "result"))
                     (embedded-triples (assoc-ref res "list-triples")))
                ;; 3.3
                (unless (json-null? object)
                  (set! list-triples
                    (cons (make-rdf-triple subject (rdf-iri "first") object)
                          list-triples)))
                ;; 3.4
                (set! list-triples
                  (cons (make-rdf-triple subject (rdf-iri "rest") rest)
                        list-triples))
                ;; 3.5
                (set! list-triples
                  (append list-triples embedded-triples)))
              (loop (cdr bnodes) (cdr lst))))
          ;; 4
          (if (null? bnodes)
              (set! result (rdf-iri "nil"))
              (set! result (car bnodes)))))
    `(("result" . ,result)
      ("list-triples" . ,list-triples))))
