Guile-JsonLD
============

Guile JsonLD is an implementation of the JsonLD API defined by the W3C for
GNU Guile.  JsonLD stands for Json for Linked Data.  Linked Data is a
representation for the semantic web.  It allows you to express links between
data, in a way that is very similar to WikiData for instance.  An object can
have relations (in the form of an IRI) that relates it to one or more objects,
in the form of a Json object or an IRI at which another JsonLD document can
usually be found, that represents that object.

JsonLD also defines different forms for a document, the expanded form being
a somewhat canonical view of the document, where everything is explicit.  The
compact form is the form that is preferred for transfering a document: instead
of explicit IRIs, it uses a less verbose format. A context is used to keep track
of things and to allow a data consummer to get an expanded document back.

This implementation tries to follow the editor's draft of
[JsonLD 1.1](https://w3c.github.io/json-ld-api) (and the final recommendation
once published).

Installing
----------

In order to install, your best option is to use the [Guix](https://guix.gnu.org)
package manager.  It can run on any existing Linux distribution, and is guaranteed
to not interact with its host distribution.  Installing Guix is as simple
as running the
[installation script](https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh).
Once installed, you can run:

```bash
guix install guile guile-jsonld
```

Otherwise, your package manager might have guile-jsonld available.  You can also
build it from source, like so:

```bash
git clone https://framagit.org/tyreunom/guile-jsonld
autoreconf -fiv
./configure
make
sudo make install
```

You will need guile, guile-json and guile-rdf for it to work.  Again, the best
way to obtain the dependencies is to use Guix from this repository:

```bash
guix environment -l guix.scm
```

`guix.scm` is a file that is provided with this repository. You can use it to
setup a development environment, as shown above, or to build the package, using
maybe a different source, like this:

```bash
guix build --with-sources=guile-jsonld=$(PWD) -f guix.scm
```

Testing
-------

The tests include running the official
[test suite](https://w3c.github.io/json-ld-api/tests/).  It requires network
access.  To run it, use:

```bash
make check
```

Please [report](https://framagit.org/tyreunom/guile-jsonld/issues) any failure!

A test report can be generated in turtle format by running:

```bash
make report
```

This executes the same tests as `make check` above, but will save test results
in the `reports` directory.  This is used by developpers to send test results
to the W3C's [conformance report](https://w3c.github.io/json-ld-api/reports).

Documentation
-------------

This section documents the API, as implemented in this library.  The API
follows the “The Application Programming Interface” section of the documentation.

### The JsonLDProcessor Interface

Our implementation of the JsonLDProcessor interface consists of the following
functions:

**Sheme Procedure**: compact input context [#:options (new-jsonld-options)]

Compacts the given input using the context according to the steps in the
Compaction algorithm.  This procedure takes an input, which can be a Json object
(as produced by the guile-json library) or a string representing the URL of a
JsonLD document.  The context is the content of the `@context` entry of the
resulting document (in the format used by the guile-json library): this can
be a simple string, an array of strings, or a JsonLD document with an `@context`
key.  The options can be used to carry additional configuration.

**Scheme Procedure**: expand input [#:options (new-jsonld-options)]

Expands the given `input` according to the steps in the Expansion algorithm.
This procedure takes an input, which can be a Json object (as produced by
the guile-jsonld library) or a string representing the URL of a JsonLD document.
The options can be used to carry information about the base URI used during
expansion, an additional context, and different options that control the behavior
of the algorithm.  See “The JsonLDOptions Type” section below.

**Scheme Procedure**: flatten input [#:context #f] [#:options (new-jsonld-options)]

Flattens the given input and optionally compacts it using the provided `context`
according to the steps in the Flattening algorithm.  This procedure takes and
input, which can be a Json object (as produced by the guile-jsonld library) or
a string representing the URL of a JsonLD document.  The options can be used
to carry additional configuration information.  An additional context can be
passed in order to run the compaction algorithm on the result.

**Scheme Procedure**: jsonld->rdf input [#:options (new-jsonld-options)]

Transforms the given input into an RDF dataset (in the format expected by
guile-rdf).  This procedure takes an input, which can be a Json object (as
produced by the guile-json library) or a string representing the URL of a JsonLD
document.  The options can be used to carry additional configuration information.

**Scheme Procedure**: rdf->jsonld dataset [#:options (new-jsonld-options)]

Transforms the given dataset into a JsonLD document in expanded form.  This
procedure takes as input an RDF dataset (as produced by the guile-rdf
library) and returns the same dataset in JsonLD format, in the expanded form.
The options can be used to carry additional configuration information.

### The JsonLDOptions Type

The `jsonld-options` type is used to pass various options to the JsonLdProcessor
methods.  This datatype is defined in the `(jsonld options)` module.

**Scheme Datatype**: jsonld-options

This type has the following fields:

* **base** (default: `#f`): The base IRI to use when expanding or compacting
  the document.  If set, this overrides the input document's IRI.
* **compact-arrays** (default: `#t`): If set to true, the JSON-LD processor
  replaces arrays with just one element with that element during compaction. If
  set to false, all arrays will remain arrays even if they have just one element. 
* **compact-to-relative** (default: `#t`): Determines if IRIs are compacted
  relative to the base option or document location when compacting.
* **document-loader** (default: `(memoize download-json)`): The callback of the
  loader to be used to retrieve remote documents and contexts, implementing the
  LoadDocumentCallback.  It is used to retrieve remote documents and contexts.
* **expand-context** (default: `#f`): A context that is used to initialize the
  active context when expanding a document.
* **extract-all-scripts** (default: `#f`): If set to `#t`, when extracting JSON-LD
  script elements from HTML, unless a specific fragment identifier is targeted,
  extracts all encountered JSON-LD script elements using an array form, if
  necessary.
* **frame-expansion** (default: `#f`): Enables special frame processing rules
  for the Expansion Algorithm.  It also enables special rules for the Serialize
  RDF as JSON-LD Algorithm to use JSON-LD native types as values, where possible.
* **ordered** (default: `#f`): If set to true, certain algorithm processing
  steps where indicated are ordered lexicographically.  If false, order is not
  considered in processing.
* **processing-mode** (default: `"json-ld-1.1"`): Sets the processing mode.  If
  set to `"json-ld-1.0"` or `"json-ld-1.1"`, the implementation must produce
  exactly the same results as the algorithms defined in this specification. If
  set to another value, the JSON-LD processor is allowed to extend or modify
  the algorithms defined in this specification to enable application-specific
  optimizations.  We do not implement any application-specific optimization.
* **produce-generalized-rdf** (default: `#t`): If set to true, the JSON-LD
  processor may emit blank nodes for triple predicates, otherwise they will be
  omitted.
* **rdf-direction** (default: `#f`): Determines how value objects containing a
  base direction are transformed to and from RDF.
  * If set to `'i18n-datatype`, an RDF literal is generated using a datatype IRI
    based on `https://www.w3.org/ns/i18n#` with both the language tag (if present)
    and base direction encoded.  When transforming from RDF, this datatype is
    decoded to create a value object containing @language (if present) and @direction.
  * If set to `'compound-literal`, a blank node is emitted instead of a literal,
    where the blank node is the subject of `rdf:value`, `rdf:direction`, and
    `rdf:language` (if present) properties.  When transforming from RDF, this
    object is decoded to create a value object containing `"@language"` (if
    present) and `"@direction"`.
* **use-native-types** (default: `#f`):
    Causes the Serialize RDF as JSON-LD Algorithm to use native JSON values in value objects avoiding the need for an explicitly @type.
useRdfType
    Enables special rules for the Serialize RDF as JSON-LD Algorithm causing rdf:type properties to be kept as IRIs in the output, rather than use @type. 
* **use-rdf-type** (default: `#f`): Enables special rules for the Serialize RDF
as JSON-LD Algorithm causing `rdf:type` properties to be kept as IRIs in the
output, rather than use @type.

Additionally, the module defines the following procedures:

**Scheme Procedure**: new-jsonld-options [#:base] [#:compact-arrays?] ...

Returns a new `jsonld-options` object with the fields filled with the corresponding
keyword passed to this procedure, or with the default value if a keyword is not
passed.

**Scheme Procedure**: update-jsonld-options options
[#:base `(jsonld-options-base options)`]
[#:compact-arrays? `(jsonld-options-compact-arrays? options)`] ...

Returns a new `jsonld-options` object with the fields filled with the
corresponding keyword passed to this procedure, or with the value found in
the required parameter `options`, another `jsonld-options` object, if the
keyword is not passed.

### Remote Document and Context Retrieval

This library implements a default document loader for JsonLD documents.  It
follows the section with the same name as this one in the JsonLD API specification.
The `(jsonld download)` module defines the following procedure:

**Scheme Procedure**: download-json uri [#:mime-type `"application/ld+json"`]
[#:extract-all-scripts? `#f`] [#:profile `#f`] [#:request-profile `#f`]
[#:http-get `http-get`]

The `download-json` defines a callback that custom document loaders have to
implement to be used to retrieve remote documents and contexts.  The callback
returns a `json-document`.  On failure, the procedure throws an exception
having an appropriate error code.

The `uri` is the location of the document and is a required parameter.  All
other parameters are optional.  A custom callback does not have to implement
the `http-get` parameter, nor the `mime-type` parameter, as they are not defined
in the specification.

If set to true, when extracting JSON-LD script elements from HTML, unless a
specific fragment identifier is targeted, `extract-all-scripts?` instructs the
callback to extract all encountered JSON-LD script elements using an array
form, if necessary.

When the resulting contentType is `text/html` or `application/xhtml+xml`, the
`profile` option determines the profile to use for selecting JSON-LD script
elements.

When set, the `request-profile` parameter contains one or more IRIs to use in
the request as a profile parameter for the requested content type.

**Scheme Datatype**: json-document

This datatype represents a json document retrieved by the callback.  This is the
type of values returned by `download-json`.  It has the following fields:

* **context-url** (default: `#f`): If available, the value of the HTTP Link
Header using the `http://www.w3.org/ns/json-ld#context` link relation in the
response.  If the response's `Content-Type` is `application/ld+json`, the HTTP
Link Header is ignored.  If multiple HTTP Link Headers using the
`http://www.w3.org/ns/json-ld#context` link relation are found, `download-json`
throws a `multiple-context-link-headers` exception.
* **document-url** (default: `#f`): The final URL of the loaded document.  This
  is important to handle HTTP redirects properly.
* **document** (default: `#f`): The retrieved document, in the form returned by
  the guile-json library.
* **content-type** (default: `#f`): The Content-Type of the loaded document,
  exclusive of any optional parameters.
* **profile** (default: `#f`): The value of any profile parameter retrieved as
  part of the original contentType.

Additionally, this module provides the following procedure:

**Scheme Procedure**: new-json-document [#:context-url] [#:document-url]
[#:document] [#:content-type] [#:profile]

Return a new `json-document` with the corresponding fields filled with the values
that were passed, or `#f` if no matching keyword is passed to the procedure.

### Error Handling

There are many JsonLD error types.  When an error is encountered, the corresponding
error code is thrown as a symbol representing this code.  The following procedure
can be used to get an English version of these errors, defined in the
`(jsonld json)` module:

**Scheme Procedure** jsonld-error->string err

Converts an error symbol to a readable English string.
