;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (test-modules testsuite)
  #:export (expected-failures
            compact-test-url
            expand-test-url
            flatten-test-url
            html-test-url
            remote-doc-test-url
            jsonld-test-url
            fromrdf-test-url
            tordf-test-url))

(define jsonld-test-url "https://w3c.github.io/json-ld-api/tests/")

(define compact-test-url
  (string-append jsonld-test-url "compact-manifest.jsonld"))

(define expand-test-url
  (string-append jsonld-test-url "expand-manifest.jsonld"))

(define flatten-test-url
  (string-append jsonld-test-url "flatten-manifest.jsonld"))

(define html-test-url
  (string-append jsonld-test-url "html-manifest.jsonld"))

(define remote-doc-test-url
  (string-append jsonld-test-url "remote-doc-manifest.jsonld"))

(define fromrdf-test-url
  (string-append jsonld-test-url "fromRdf-manifest.jsonld"))

(define tordf-test-url
  (string-append jsonld-test-url "toRdf-manifest.jsonld"))

(define expected-failures
  `(("https://w3c.github.io/json-ld-api/tests/html-manifest.jsonld#te010" .
       "entities are not preserved by xml->sxml")
    ("https://w3c.github.io/json-ld-api/tests/html-manifest.jsonld#te015" .
       "HTML tags in scripts are not ignored by xml->sxml")
    ("https://w3c.github.io/json-ld-api/tests/html-manifest.jsonld#te016" .
       "HTML tags in scripts are not ignored by xml->sxml")
    ("https://w3c.github.io/json-ld-api/tests/html-manifest.jsonld#te017" .
       "HTML tags in scripts are not ignored by xml->sxml")
    ))
